import { Component, OnInit } from '@angular/core';
import { Education } from 'src/app/models/education.model';
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'cv-educations',
    templateUrl: './educations.component.html',
    styleUrls: ['./educations.component.scss']
})
export class EducationsComponent implements OnInit {

    educations: Education[] = [];
    educationsList: any[] = [];

    constructor(private dataSvc: DataService) { }

    ngOnInit() {
        this.dataSvc.getEducations().subscribe((data: Education[]) => {
            this.educations = data;

            this.educations.map(s => {
                if (this.educationsList.filter(o => o.name === s.type).length > 0) {
                    this.educationsList.filter(o => o.name === s.type)[0].educations.push(s);
                } else {
                    const item = {
                        name: s.type,
                        educations: [
                            {
                                title: s.title,
                                type: s.type,
                                school: s.school,
                            }
                        ]
                    };
                    this.educationsList.push(item);
                }
            });

            console.log(this.educationsList);
        });
    }
}
