import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Education } from 'src/app/models/education.model';

@Component({
    selector: 'cv-education',
    templateUrl: './education.component.html',
    styleUrls: ['./education.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class EducationComponent implements OnInit {

    @Input() education: Education;

    constructor() { }

    ngOnInit() {
    }

}
