import { Component, OnInit } from '@angular/core';
import { Hobby } from 'src/app/models/hobby.model';
import { DataService } from 'src/app/services/data.service';

@Component({
    selector: 'cv-hobbies',
    templateUrl: './hobbies.component.html',
    styleUrls: ['./hobbies.component.scss']
})
export class HobbiesComponent implements OnInit {

    public hobbies: Hobby[] = [];

    constructor(private dataSvc: DataService) { }

    ngOnInit() {
        this.dataSvc.getHobbies().subscribe((data: Hobby[]) => {
            this.hobbies = data;
        });
    }
}
