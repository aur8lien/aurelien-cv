import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Experience } from 'src/app/models/experience.model';

@Component({
    selector: 'cv-experience',
    templateUrl: './experience.component.html',
    styleUrls: ['./experience.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ExperienceComponent implements OnInit {

    @Input() experience: Experience;

    constructor() { }

    ngOnInit() {
    }
}
