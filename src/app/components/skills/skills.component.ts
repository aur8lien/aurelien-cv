import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Skill, SkillCategory } from 'src/app/models/skill.model';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'cv-skills',
    templateUrl: './skills.component.html',
    styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

    skills: Skill[] = [];
    skillsList: any = [];

    order: string[] = ['cti', 'dev', 'web', 'tools'];

    constructor(private dataSvc: DataService, private route: ActivatedRoute) { }

    ngOnInit() {
        this.dataSvc.getSkills().subscribe((data: Skill[]) => {
            this.skills = data;

            this.skills.map(s => {
                if (this.skillsList.filter(o => o.name === s.category).length > 0) {
                    this.skillsList.filter(o => o.name === s.category)[0].skills.push(s);
                } else {
                    const skillset = {
                        name: s.category,
                        skills: [
                            {
                                name: s.name,
                                category: s.category,
                                proficiency: s.proficiency,
                                icon: s.icon,
                                description: s.description
                            }
                        ]
                    };
                    this.skillsList.push(skillset);
                }
            });

            this.orderSkills();

        });
    }

    public getSkillsByCategory(category: SkillCategory) {
        return this.skills.filter(o => o.category === category);
    }

    private orderSkills() {
        const newArray: any[] = [];

        this.order.forEach(o => {
            newArray.push(this.skillsList.filter(s => s.name === o)[0]);
        });

        this.skillsList = newArray;
    }
}
