import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'cv-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    constructor(private matIconReg: MatIconRegistry, private domSanitizer: DomSanitizer) {
        this.matIconReg.addSvgIcon('linkedin', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/linkedin.svg'));
        this.matIconReg.addSvgIcon('nz', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/nz.svg'));
    }

    ngOnInit() {

    }

}
