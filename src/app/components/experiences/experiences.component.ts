import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Experience } from 'src/app/models/experience.model';

@Component({
    selector: 'cv-experiences',
    templateUrl: './experiences.component.html',
    styleUrls: ['./experiences.component.scss']
})
export class ExperiencesComponent implements OnInit {

    experiences: Experience[] = [];

    constructor(private dataSvc: DataService) { }

    ngOnInit() {
        this.dataSvc.getExperiences().subscribe((data: Experience[]) => {
            this.experiences = data;
            console.log(data);
        });
    }

}
