import { Component, OnInit, Input } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Skill } from 'src/app/models/skill.model';

@Component({
    selector: 'cv-skill',
    templateUrl: './skill.component.html',
    styleUrls: ['./skill.component.scss']
})
export class SkillComponent implements OnInit {

    @Input() skill: Skill;

    constructor(private matIconReg: MatIconRegistry, private domSanitizer: DomSanitizer) {
        this.matIconReg.addSvgIcon('java', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/java.svg'));
        this.matIconReg.addSvgIcon('javascript', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/javascript.svg'));
        this.matIconReg.addSvgIcon('html', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/html.svg'));
        this.matIconReg.addSvgIcon('css', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/css.svg'));
        this.matIconReg.addSvgIcon('angular', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/angular.svg'));
        this.matIconReg.addSvgIcon('ionic', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/ionic.svg'));
        this.matIconReg.addSvgIcon('csharp', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/csharp.svg'));
        this.matIconReg.addSvgIcon('material', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/material.svg'));
        this.matIconReg.addSvgIcon('bootstrap', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/bootstrap.svg'));
        this.matIconReg.addSvgIcon('genesys', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/genesys.svg'));
        this.matIconReg.addSvgIcon('cisco', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/cisco.svg'));
        this.matIconReg.addSvgIcon('avaya', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/avaya.svg'));
        this.matIconReg.addSvgIcon('vxml', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/vxml.svg'));
        this.matIconReg.addSvgIcon('db', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/database.svg'));
        this.matIconReg.addSvgIcon('code', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/code.svg'));
        this.matIconReg.addSvgIcon('ps', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/ps.svg'));
        this.matIconReg.addSvgIcon('3ds', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/3ds.svg'));
        this.matIconReg.addSvgIcon('eclipse', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/eclipse.svg'));
        this.matIconReg.addSvgIcon('netbeans', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/netbeans.svg'));
        this.matIconReg.addSvgIcon('vocalcom', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/vocalcom.svg'));
        this.matIconReg.addSvgIcon('nuance', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/nuance.svg'));
        this.matIconReg.addSvgIcon('inprod', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/inprod.svg'));
        this.matIconReg.addSvgIcon('cyara', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/cyara.svg'));
    }

    ngOnInit() {
    }
}
