import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'cv-hobby',
    templateUrl: './hobby.component.html',
    styleUrls: ['./hobby.component.scss']
})
export class HobbyComponent implements OnInit {

    @Input() hobby: any;

    constructor() { }

    ngOnInit() {
    }
}
