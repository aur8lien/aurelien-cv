import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SkillsComponent } from './components/skills/skills.component';
import { ExperiencesComponent } from './components/experiences/experiences.component';
import { EducationsComponent } from './components/educations/educations.component';
import { HobbiesComponent } from './components/hobbies/hobbies.component';


const routes: Routes = [
    {
        path: 'skills',
        component: SkillsComponent
    },
    {
        path: 'professional',
        component: ExperiencesComponent
    },
    {
        path: 'education',
        component: EducationsComponent
    },
    {
        path: 'hobbies',
        component: HobbiesComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
