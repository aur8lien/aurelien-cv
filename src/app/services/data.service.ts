import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Skill } from '../models/skill.model';
import { Observable } from 'rxjs';
import { Experience } from '../models/experience.model';
import { Education } from '../models/education.model';
import { Hobby } from '../models/hobby.model';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    constructor(private http: HttpClient) { }

    public getSkills(): Observable<Skill[]> {
        return this.http.get<Skill[]>('https://aurelien-api.herokuapp.com/skills');
    }

    public getExperiences(): Observable<Experience[]> {
        return this.http.get<Experience[]>('https://aurelien-api.herokuapp.com/experiences');
    }

    public getEducations() {
        return this.http.get<Education[]>('https://aurelien-api.herokuapp.com/educations');
    }


    public getHobbies() {
        return this.http.get<Hobby[]>('https://aurelien-api.herokuapp.com/hobbies');
    }
}
