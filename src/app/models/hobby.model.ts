import { Project } from './project.model';

export interface Hobby {
    name: string;
    environment?: string;
    project?: Project[];
}
