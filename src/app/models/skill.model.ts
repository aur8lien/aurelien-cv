export type SkillCategory = 'dev' | 'software' | 'cti' | 'web' | 'other';

export interface Skill {
    name: string;
    icon: string;
    category: SkillCategory;
    proficiency: number;
    description: string;
}
