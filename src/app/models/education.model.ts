import { Project } from './project.model';

export type EducationType = 'school' | 'traiging';

export interface Education {
    title: string;
    school: string;
    type: EducationType;
}
