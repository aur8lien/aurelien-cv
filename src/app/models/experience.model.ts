import { Project } from './project.model';

export interface Experience {
    logo: string;
    position: string;
    company: string;
    from: string;
    to: string;
    environment: string;
    description: string;
    projects: Project[];
}
