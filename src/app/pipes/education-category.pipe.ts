import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'educationCategory'
})
export class EducationCategoryPipe implements PipeTransform {

    transform(value: string): string {
        switch (value) {
            case 'school':
                return 'Education';
            case 'training':
                return 'Training';
        }
    }

}
