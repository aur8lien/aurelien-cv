import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'skillCategory'
})
export class SkillCategoryPipe implements PipeTransform {

    transform(value: string): string {
        switch (value) {
            case 'web':
                return 'Web Design';
            case 'dev':
                return 'Development';
            case 'other':
                return 'Other';
            case 'cti':
                return 'Contact Centre';
            case 'tools':
                return 'Tools';
        }
    }

}
